import requests, json
baseURL="https://snowfall.weatherdb.com/ajax_search?_len=20&page=0&app_id=3455&_sortfld=distance&_sortdir=ASC&_fil%5B0%5D%5Bfield%5D=_location&_fil%5B0%5D%5Boperator%5D=NEAR&_fil%5B0%5D%5Bvalue%5D%5Blatitude%5D="
baseURL2 = "&_fil%5B0%5D%5Bvalue%5D%5Blongitude%5D="
headers = {'User-agent':'Mozilla/5.0'}

def getSnowData(lat,lng):
	URL = baseURL+str(lat)+baseURL2+str(lng)
	response = requests.get(URL, headers=headers)
	result = json.loads(response.content.decode('utf-8'))
	if len(result['data']['data']):
		#print("look hereer")
		#print(result['data'])
		return result['data']['data'][0][2]['raw']
	else:
		return None
	
#print(getSnowData(45.0541811,-70.3085109))