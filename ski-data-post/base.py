import io
import json
import csv as csv
from yelpSearch import yelpApiSearchLatLng
from googleTextQuery import gMapQueryType
from googleTextQuery import gMapDistance
from weatherdb import getSnowData
from wapiQuerry import station_wapi_finder
from distance_calc import haversine

yelpRadius = 10
inputFile = 'skiing-us-input.csv'
outputFile = 'skiing-us-output.csv'

csv_Input = csv.reader(open(inputFile),dialect='excel')
print('file in ok')	
list_Input = list(csv_Input)
firstline = True


def getAirportDistance(name, city, state,lat,lng):
	airportLatLng = gMapQueryType(name,city,state,'near','airport')
	if airportLatLng['lat'] != None:
		# origin = str(lat)+','+str(lng)
		# destination = str(airportLatLng['lat'])+','+str(airportLatLng['lng'])
		# print ((origin, destination))
		# distance = gMapDistance(origin, destination)
		
		distance = int(haversine(lng, lat, airportLatLng['lng'], airportLatLng['lat']))
	else:
		distance = None
	return distance
	
def getBusTrainDistance(lat,lng):
	if station_wapi_finder(lat,lng) != None:
		lnglat = station_wapi_finder(lat,lng)
		lat2 = lnglat['lat']
		lon2 = lnglat['lng']
		distance = int(haversine(lng, lat, lon2, lat2))
	else:
		distance = None
	return distance

with open(outputFile, 'w') as skiingOutput:
	csv_writer = csv.writer(skiingOutput,dialect='excel')	
	print('file out ok')	
	for resort in list_Input:
		if firstline:
			firstline = False
		else:
			resortName = resort[0]
			resortCity = resort[4]
			resortState = resort[3]
			latlng = gMapQueryType(resortName, resortCity, resortState)
			resort[5] = latlng['lat']
			resort[6] = latlng['lng']
			if resort[5] != None:
				print('got lat long')
				resort[11] = getAirportDistance(resortName, resortCity, resortState,resort[5],resort[6])
				print('airport ok')
				resort[12] = getBusTrainDistance(resort[5],resort[6])
				print('bus ok')
				resort[10] = yelpApiSearchLatLng('hotels,bedbreakfast,guesthouses,hostels,resorts',resort[5],resort[6],yelpRadius)
				print('yelp lodging ok')
				resort[13] = yelpApiSearchLatLng('restaurants',resort[5],resort[6],yelpRadius)
				print('yelp rest ok')
				resort[14] = yelpApiSearchLatLng('shopping',resort[5],resort[6],yelpRadius)
				print('yelp shop ok')
				resort[7] = getSnowData(resort[5],resort[6])
				print('snow ok')
		print(resort)
		csv_writer.writerow(resort)