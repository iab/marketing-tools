from yelp.client import Client
from yelp.oauth1_authenticator import Oauth1Authenticator
import io
import json

# read API keys
auth = Oauth1Authenticator(
    consumer_key='qcl9CCqYR7LVBJMAD4Hyew',
    consumer_secret='frtzJbsfN0LaFQTy7lUylR9HiTk',
    token='Y8-RKyusYNdw7AVlh2LEq1BKIqFHJG2v',
    token_secret='IXICsSyN_fM0dD1yc8Y5RSkCLWY'
)
def yelpApiSearch(category,city):
	client = Client(auth)
	params = {	'category_filter': category,
				'location' :city
				}
	response = client.search(**params)
	return response.total

def yelpApiSearchLatLng(category,lat,lng,radius):
	client = Client(auth)
	params = {	'category_filter': category,
				'latitude' : lat,
				'longitude': lng,
				'radius_filter':radius*1000*1.609
				}
	response = client.search_by_coordinates(**params)
	return response.total

	
#print(yelpApiSearchLatLng('hotels,bedbreakfast,guesthouses,hostels,resorts',44.0827817,-71.2293986,10))