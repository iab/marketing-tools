import requests
import json
from sys import exit

def station_wapi_finder(lat, lng):
	apiUrl = 'https://api-dev.wanderu.com/v2/'
	authEndPoint = 'auth.json'
	queryEndPoint = 'stations.json'

	queryResp = []
	exportTXT = []

	headers = {"content-type": "application/json"}
	credentials = {"credentials": {"type": "anonymous"}}

	try:
		authResponse = requests.post(apiUrl+authEndPoint,headers=headers, data=json.dumps(credentials))
	except requests.exceptions.RequestException as e:
		print ("issues with auth request " + str(e))
		sys.exit(1)

	auth = authResponse.json()
	token = auth['result']['token']

	headersRequest = {'X-TOKEN': token}
	payload = {	
					'lat': lat, 
					'lng': lng,
					'dist':160000
			}
				
	queryBody = requests.get(apiUrl+queryEndPoint, headers = headersRequest, params=payload)
	#if ('Response [200]' in queryBody):
	queryResp = queryBody.json()
	if len(queryResp['result']):
		lnglat = (queryResp['result'][0]['loc'])
	
	else:
		lnglat = None
	return lnglat
