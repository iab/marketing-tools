WITH amt_txns AS (
    SELECT        d_stations.wcityid + '_' + a_stations.wcityid route,
               
              COUNT(*) txn_count
    FROM      report.wanderu_pgtxn pgtxn
    JOIN      report.wanderu_stations d_stations
              ON  pgtxn.depart_id = d_stations.wid
    JOIN      report.wanderu_stations a_stations
              ON  pgtxn.arrive_id = a_stations.wid
    WHERE     carrier in ('AMT','USACL')
           
    GROUP BY  1
    HAVING    COUNT(*) > 10
),
train_c2c AS (
    SELECT    wid,
              url+'/' url,
              routes
    FROM      report.seo_city_to_city
    WHERE     carriers LIKE ('%AMT%')
              OR carriers LIKE ('%USACL%')
), 
amt_pageviews AS (
    SELECT    RIGHT(page_urlpath,LEN(page_urlpath)-18) url_trim,
              COUNT(*) page_views
    FROM      report.events events
    WHERE     page_urlpath LIKE ('%/train-tickets/%')
    GROUP BY  1, page_urlpath
    HAVING    COUNT(*)>80 and LEN(page_urlpath) > 18
),
combined AS (
    SELECT    wid,
              url,
              txn_count,
              page_views,
              CASE WHEN (txn_count > 1 OR page_views > 1) THEN 1 ELSE NULL END active
              

    FROM      train_c2c
    LEFT OUTER JOIN   amt_txns
              ON train_c2c.wid = amt_txns.route
    LEFT OUTER JOIN   amt_pageviews
              ON train_c2c.url = amt_pageviews.url_trim
)

SELECT    wid
FROM      combined    
WHERE     active = 1
