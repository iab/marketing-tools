import precache_sql_query
import unicodedata
import json
import precache_sql_query

SQL_YOU_WANT_TO_USE = open("amt-new-precache.sql", "r").read()
carrierWidfile = 'amt-wid-map.json'

# def cityStationMapper():
	# map = {}
	# path_to_json = 'stations-gen.json'
	# with open(path_to_json) as json_file:
		# for station in json.load(json_file):
			# wid = station['wid']
			# wcityid = station['wcityid']
			# if wcityid not in map:
				# map[wcityid] = [wid]
			# elif wcityid in map:
				# map[wcityid].append(wid)
	# return map

def load_stations():
	path_to_json = 'stations-gen.json'
	stationsCities = {}
	with open(path_to_json) as json_file:
		for station in json.load(json_file):
			wid = station['wid']
			wcityid = station['wcityid']
			stationsCities[wid] = wcityid
	return stationsCities

def cleanSQLData(data):
	cleanData = []
	for row in data:
		cleanData.append(row['wid'])
	return cleanData
	
def precacheCreator():
	cityMap = load_stations()
	data = precache_sql_query.getWarehouseQuery(SQL_YOU_WANT_TO_USE)
	cityRoutes = cleanSQLData(data)
	map = {}
	counter = 0
	with open(carrierWidfile) as json_file:
		for departWID, stations in json.load(json_file).items():
			departCity = cityMap[departWID]
			for arriveWID in stations:
				arriveCity = cityMap[arriveWID]
				if departCity+'_'+arriveCity in cityRoutes:
					print('found one ' + str(departCity+'_'+arriveCity))
					counter = counter+1
					if departWID in map: 
						map[departWID].append(arriveWID)
					else:
						map[departWID] = [arriveWID]
	print(counter)
	return map
	
with open('amt_precache.json', 'w') as outfile:
    json.dump(precacheCreator(), outfile, indent=2)
# cityRouteMapper(data, cityMap)
	
#create map for each city route of stations
#then cycle through AMT route map looking for match if found keep it


## cycle through and get value w batches of 50 
# come back and assign value back to app wid and mark if duplicate