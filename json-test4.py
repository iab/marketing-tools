import pandas as pd
import numpy as np
from itertools import groupby 
from collections import OrderedDict, defaultdict
import json

df = pd.read_csv('homepage-link-flow-react.csv', encoding = "ISO-8859-1")
df = df.replace(np.nan, False, regex=True)

commonNames = {'New York':'New York City (NYC)'}
results = {}

for (locale), bag in df.groupby(["locale"]):
    print(locale)
    results[locale] = {
        'title': 'link-flow-title',
        'tabs': [],
        'tabData':{}
    }
    locale_df = bag.drop(["locale"], axis=1)
    i = 0
    for (tabLabel), locale_df in bag.groupby(["tab_label"]):
        
        results[locale]['tabs'].append(
            {
               'id': 'tab'+str(i),
               'text': tabLabel
            }
        )
        
        results[locale]['tabData']['tab'+str(i)] = []
        
        transMode_df = locale_df.drop(["tab_label"], axis=1)
        for (seedCity), seedCity_df in transMode_df.groupby(["grouping_label"]):
            
            cityList = []
            
            city_df = seedCity_df.drop(["grouping_label"], axis=1)
            for (url), link_df in city_df.groupby(["url"]):
                linkDict = link_df.to_dict('records')[0]
                
                linkInfo = {
                           'url': url,
                           'left': {
                               'key': linkDict["key"],       
                                }
                            }   
                if (linkDict["isuireact"]):
                    linkInfo['isUiReact'] = linkDict["isuireact"]
                    
                if (linkDict["replace1"] and (linkDict["replace2"])):
                    if linkDict["replace1"] in commonNames:
                        linkDict["replace1"] = commonNames[linkDict["replace1"]]
                    if linkDict["replace2"] in commonNames:
                        linkDict["replace2"] = commonNames[linkDict["replace2"]]
                    linkInfo['left']['replacements'] =  {
                        'replace1': linkDict["replace1"],
                        'replace2': linkDict["replace2"]            
                        }
                if (linkDict["price"]):
                    linkInfo['right'] = linkDict["price"]
                
                
                cityList.append(linkInfo)
                
            #print(cityList)
            
            results[locale]['tabData']['tab'+str(i)].append (
               {
                  'header': seedCity,
                  'data': cityList     
               }
            )
            
            
        i += +1
    
    # subset = [OrderedDict(row) for i,row in contents_df.iterrows()]
    # results.append(OrderedDict([("zipcode", zipcode),
                                # ("state", state),
                                # ("subset", subset)]))

# print(results)
print (json.dumps(results, indent=4))
with open('homepage-link-flow-react.json', "w") as f:
        f.write(json.dumps(results, sort_keys=False, indent=4, separators=(',', ': '),ensure_ascii=False))