import networkx as nx
import c2c_sql_query
import csv
import os

def generate_in_tuple(keys):
    quoted_keys = ["'{}'".format(key) for key in keys]
    return "({})".format(', '.join(quoted_keys))

cities = ('Anaheim, CA, US', 'San Francisco, CA, US', 'St Louis, MO, US', 'Phoenix, AZ, US', 'New York, NY, US', 'Philadelphia, PA, US', 'Detroit, MI, US', 'Denver, CO, US', 'Los Angeles, CA, US', 'Boston, MA, US', 'Cincinnati, OH, US', 'Chicago, IL, US', 'Kansas City, MO, US', 'Miami, FL, US', 'Milwaukee, WI, US', 'Houston, TX, US', 'Washington, DC, US', 'Baltimore, MD, US', 'San Diego, CA, US', 'Pittsburgh, PA, US', 'Cleveland, OH, US', 'Toronto, OH, US', 'Seattle, WA, US', 'Minneapolis, MN, US', 'Dallas, TX, US', 'Atlanta, GA, US', 'Tampa, FL, US') 
	
#cities = ('Charleston, SC, US', 'Savannah, GA, US', 'Orlando, FL, US', 'Atlanta, GA, US') 
#cities = ('Cannes, , FR', 'Rome, RM, IT', 'Lyon, , FR', 'Nice, , FR', 'Milan, MI, IT', 'Venice, VE, IT', 'Marseille, , FR', 'Geneva, GE, CH', 'Florence, FI, IT')
citiesTuple = generate_in_tuple(cities)
sql_custom =( 	"""WITH base as ( 
					SELECT    	depart_cityname +', '+ ISNULL(depart_state,'')+', '+ depart_country depart_cityname, 
								  arrive_cityname +', '+ ISNULL(arrive_state,'')+', '+ arrive_country arrive_cityname,
								  AVG(min_price_usd) min_price,
								  ROW_NUMBER() OVER (PARTITION BY depart_cityname +', '+ ISNULL(depart_state,'')+', '+ depart_country ORDER BY min_price ASC) as connection_rank
					FROM      		report.compressed_trips 
					WHERE     		depart_cityname +', '+ ISNULL(depart_state,'')+', '+ depart_country in {}
								  and arrive_cityname +', '+ ISNULL(arrive_state,'') +', '+ arrive_country in  {}
								  AND depart_cityname +', '+ ISNULL(depart_state,'')+', '+ depart_country != arrive_cityname +', '+ ISNULL(arrive_state,'') +', '+ arrive_country
								  AND depart_date between GETDATE() - Interval '30 days' AND GETDATE() 
					GROUP BY  1,2 
					)
					SELECT    depart_cityname, 
							  arrive_cityname,
							  min_price
					FROM      base
					WHERE     connection_rank <= 10"""
			).format(cities, cities)

			
			
data = c2c_sql_query.getWarehouseQuery(sql_custom)


print('got data ok')


def WriteDictToCSV2(csv_file,csv_columns,dict_data):
	with open(csv_file, 'wb') as f:  # Just use 'w' mode in 3.x
		w = csv.DictWriter(f, fieldnames=csv_columns)
		w.writerows(dict_data)
	return 

def WriteDictToCSV(csv_file,csv_columns,dict_data):
	try:
		with open(csv_file, 'wb') as csvfile:
			writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
			writer.writeheader()
			for data in dict_data:
				writer.writerow(data)
	except IOError as errno:
		print("I/O error({0}): {1}".format(errno, strerror))    
	return 


G=nx.DiGraph()
for city in cities:
	print(city)
	G.add_node(city)

for row in data:
	departCity = row['depart_cityname']
	arriveCity = row['arrive_cityname']
	cost = row['min_price']
	G.add_edge(departCity, arriveCity, weight=cost)

nodeCount = nx.number_of_nodes(G)	
lenG = len(G)
distances=[]
stop = False
print(nodeCount)
print(lenG)

print(G.edges.data())




#print('simple cycle')
#a = list(nx.simple_cycles(G))
#a.sort(key=len, reverse=true)

#print(a)


#go through each type of cycle 
while lenG > 2 and stop == False:
	print('lenG')
	print(lenG)
	for i in cities:
		#cutoff is to make sure we get out to the ham. cycle
		print(i)
		for path in nx.all_simple_paths(G, source=i, target=i, cutoff=lenG):
			#we only want the longest ones
			print(path)
			print(len(path))
			if(len(path)==lenG+1):
				distance=0
				
				#add up the weights for each edge
				for i in range(0,lenG):
					distance=distance+G[path[i]][path[i+1]]['weight']
					
				#add these to a list
				distances.append((distance,path))
				
		print(len(distances))		
		if len(distances) > 0:
			stop = True
		elif len(distances) == 0:
			lenG = lenG -1
#defaults
print('route length' + str(lenG) + 'length of distances ' + str(len(distances)))
min_dist=distances[0][0];
min_path=[]

#find the min distance
for dist in distances:
    if(dist[0]<=min_dist):
        min_dist=dist[0]
        min_path=dist[1]

print(min_path)
print("has a cost of")
print(min_dist)

routes = []
first_pass = 0
previous_city = ''
for city in min_path:
	print(city)
	if first_pass == 0:
		first_pass = 1
	elif first_pass == 1:
		routes.append(previous_city + ' - ' + city)
	previous_city = city
	
print(routes)

routes = generate_in_tuple(routes)
sql_custom2 =( 	"""WITH base AS (
						SELECT    depart_cityname +', '+ ISNULL(depart_state,'')+', '+ depart_country + ' - ' + arrive_cityname +', '+ ISNULL(arrive_state,'')+', '+ arrive_country route,
								  MIN(min_price_usd) min_price
						FROM      report.compressed_trips 
						WHERE     depart_cityname +', '+ ISNULL(depart_state,'')+', '+ depart_country + ' - ' + arrive_cityname +', '+ ISNULL(arrive_state,'')+', '+ arrive_country in {}
								  AND
								  depart_date between GETDATE() - Interval '30 days' AND GETDATE()
						GROUP BY  1
					)
					SELECT  base.route,
							trip_count,
							ct.carrier,
							DATE_PART(WEEKDAY,ct.depart_date) day_of_week,	
							MIN(base.min_price) min_price,
							MIN(ct.avg_price_usd)	min_avg_price,
							AVG(ct.avg_price_usd)	avg_price,		
							MIN(ct.avg_duration) avg_duration 
					FROM    base
					JOIN    report.compressed_trips ct
					ON      base.route = ct.depart_cityname +', '+ ISNULL(ct.depart_state,'')+', '+ ct.depart_country + ' - ' + ct.arrive_cityname +', '+ ISNULL(ct.arrive_state,'')+', '+ ct.arrive_country
							AND base.min_price = ct.min_price_usd
					WHERE   depart_cityname +', '+ ISNULL(depart_state,'')+', '+ depart_country + ' - ' + arrive_cityname +', '+ ISNULL(arrive_state,'')+', '+ arrive_country in {}
							AND depart_date between GETDATE() - Interval '30 days' AND GETDATE()  
					GROUP BY  1,2,3,4"""
				).format(routes, routes)

			
			
data2 = c2c_sql_query.getWarehouseQuery(sql_custom2)

print(data2)

#WriteDictToCSV('route-finder-output.csv',['route','trip_count','min_price','avg_price','day_of_week','avg_duration','carrier'],data2)
WriteDictToCSV2('route-finder-output.csv',['route','trip_count','min_price','avg_price','day_of_week','avg_duration','carrier']	,data2)