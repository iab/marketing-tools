import networkx as nx
import c2c_sql_query
from googleTextQuery import gMapTextQueryCityTranslated
import csv
import os

def generate_in_tuple(keys):
    quoted_keys = ["'{}'".format(key) for key in keys]
    return "({})".format(', '.join(quoted_keys))

PoIs = ['La Grand Place','Atomium','St. Alexander Nevski Cathedral','Rila Monastery','Ancient City of Nessebar','St. Lawrence Fortress','Charles Bridge','Prague Astrological Clock','The Little Mermaid Copenhagen','The Bastille','Eiffel Tower','Arc de Triomphe','Notre Damme','Disneyland Paris','Brandenburg Gate','Reichstag Building','Berlin Wall','Parthenon','Acropolis','Hungarian Parliament Building','Blarney Stone','Grand Canal Rialto Bridge','Trevi Fountain','Pantheon','Colosseum','Leaning Tower of Pisa','St. Peter\'s Basillica','Siena Cathedral','Il Duomo di Firenze','Sistine Chapel','Anne Frank House','Wawel Castle','Tower of Belem','Bran Castle','St Basil\'s Cathedral','Red Square','Alhambra','El Escorial','Mosque of Córdoba','Alcázar of Seville','Santiago de Compostela Cathedral','Sagrada Família','Grand Hotel Stockholm','Basilica Cistern','Blue Mosque','Ephesus','Big Ben','Buckingham Palace','Tower Bridge','Stonehenge','Admiralty Arch','Tower of London','The British Museum','Stirling Castle']

latlng = []
latlng_dict = {}

for poi in PoIs:
	location = gMapTextQueryCityTranslated(poi,'point_of_interest')
	if location['latlng'] in latlng_dict:
		latlng_dict[location['latlng']]['pois'].append(poi)
	else:
		latlng_dict[location['latlng']] = {}
		latlng_dict[location['latlng']]['city'] = location['city']
		latlng_dict[location['latlng']]['country'] = location['country']
		latlng_dict[location['latlng']]['pois'] = []
		latlng_dict[location['latlng']]['pois'].append(poi)
	if location['latlng'] not in latlng:
		latlng.append(location['latlng'])
	
	print(poi + ' - ' + str(location))
	
print(latlng_dict)

latlngTuple = generate_in_tuple(latlng)	

print(latlngTuple)

sql_custom =(
				"""
				WITH lookup AS (
				  SELECT  to_char(google_latitude, 'FM999999990.0') || ',' || to_char(google_longitude, 'FM999999990.0') latlng,
						  google_name
				  FROM    report.wanderu_cities
				  WHERE to_char(google_latitude, 'FM999999990.0') || ',' || to_char(google_longitude, 'FM999999990.0') in {} 
				)

				SELECT       depart_cityname,
							 arrive_cityname,
							 d_look.latlng d_latlng,
							 a_look.latlng a_latlng,
							 MIN(min_price_usd) min_price 
					FROM      report.compressed_trips ct
					JOIN      lookup d_look
					on        ct.depart_cityname = d_look.google_name
					JOIN      lookup a_look
					on        ct.arrive_cityname = a_look.google_name
					WHERE     depart_country not in ('US','MX','CA')
							  
					GROUP BY  1,2,3,4
				"""
			).format(latlngTuple)			
			
data = c2c_sql_query.getWarehouseQuery(sql_custom)

print(data)

G=nx.DiGraph()
for thing in latlng:
	G.add_node(thing)

for row in data:
	departCity = row['d_latlng']
	arriveCity = row['a_latlng']
	cost = row['min_price']
	G.add_edge(departCity, arriveCity, weight=cost)

nodeCount = nx.number_of_nodes(G)	
lenG = len(G)
distances=[]
stop = False

print(nodeCount)
print(lenG)
#go through each type of cycle 
while lenG > 2 and stop == False:
	for i in latlng:
		#cutoff is to make sure we get out to the ham. cycle
		print(i)
		for path in nx.all_simple_paths(G, source=i, target=i, cutoff=lenG):
			#we only want the longest ones
			
			if(len(path)==lenG+1):
				distance=0
				
				#add up the weights for each edge
				for i in range(0,lenG):
					distance=distance+G[path[i]][path[i+1]]['weight']
					
				#add these to a list
				distances.append((distance,path))
				
		if len(distances) > 0:
			stop = True
		elif len(distances) == 0:
			lenG = lenG -1

# defaults
min_dist=distances[0][0];
min_path=[]

# find the min distance
for dist in distances:
    if(dist[0]<=min_dist):
        min_dist=dist[0]
        min_path=dist[1]

min_path_dict = {}		
for city in min_path:
	cityname = latlng_dict[city]['city']
	PoIs = latlng_dict[city]['pois']
	min_path_dict[city] = (cityname,PoIs)
		
print(min_path_dict)	
print("has a cost of")
print(min_dist)

routes = []
first_pass = 0
previous_city = ''
for entry in min_path_dict:
	print(entry)
	city = min_path_dict[entry][0]
	print(city)
	if first_pass == 0:
		first_pass = 1
	elif first_pass == 1:
		routes.append(previous_city + ' - ' + city)
	previous_city = city
	
print(routes)

routes = generate_in_tuple(routes)
sql_custom2 =( 	"""WITH base AS (
						SELECT    depart_cityname + ' - ' + arrive_cityname route,
								 MIN(min_price_usd) min_price
						FROM      report.compressed_trips
						WHERE     depart_country not in ('US','MX','CA')
								  and depart_cityname + ' - ' + arrive_cityname in {}
						GROUP BY  1
					)
					SELECT  base.route,
							ct.carrier,
							MIN(base.min_price),
							DATE_PART(WEEKDAY,ct.depart_date) day_of_week,
							MIN(ct.avg_duration) avg_duration,
							trip_count
							
					FROM    base
					JOIN    report.compressed_trips ct
					ON      base.route = ct.depart_cityname + ' - ' + ct.arrive_cityname AND base.min_price = ct.min_price_usd
					WHERE     ct.depart_country not in ('US','MX','CA')
								  and ct.depart_cityname + ' - ' + ct.arrive_cityname in {}
					GROUP BY  1,2,4,6"""
				).format(routes, routes)

			
			
data2 = c2c_sql_query.getWarehouseQuery(sql_custom2)

print(data2)

WriteDictToCSV('route-finder-POI-output.csv',('route','trip_count','min','day_of_week','avg_duration','carrier'),data2)
