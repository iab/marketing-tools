import requests, json
baseURL="https://maps.googleapis.com/maps/api/place/textsearch/json?query="
placeID_URL = "https://maps.googleapis.com/maps/api/place/details/json?placeid="
key = "AIzaSyDsrstTju62D1jC-winYbW_vo3DjV1yXxA"
headers = {'User-agent':'Mozilla/5.0'}

def gMapTextQuery(query,qualifier=None):
	URL = baseURL+query+'&key='+key+'&type='+str(qualifier)+'&rankby=prominence'
	response = requests.get(URL, headers=headers)
	result = json.loads(response.content.decode('utf-8'))
	#print (result)
	if result['results']:
		return result['results'][0]['geometry']['location']
	else:
		return {'lat': None, 'lng': None}

def gMapPlaceLookup(placeid):
	URL = placeID_URL+placeid+'&key='+key+'&language=en-GB'
	response = requests.get(URL, headers=headers)
	result = json.loads(response.content.decode('utf-8'))
	city = ''
	country = ''
	#print(result['result']['geometry']['location'])
	for addyComponent in result['result']['address_components'] :
		if addyComponent['types'][0] == 'locality':
			city = addyComponent['short_name']
		elif addyComponent['types'][0] == 'country':
			country = addyComponent['short_name']
		latlng = str(round(result['result']['geometry']['location']['lat'],1)) + ',' + str(round(result['result']['geometry']['location']['lng'],1))
	return{'city':city,'country':country,'latlng':latlng}
	
	
def gMapTextQueryPlaceDetails(query, qualifier = None):
	URL = baseURL+query+'&key='+key+'&type='+str(qualifier)+'&rankby=prominence'
	response = requests.get(URL, headers=headers)
	result = json.loads(response.content.decode('utf-8'))
	#print (result)
	if result['results']:
		return(gMapPlaceLookup(result['results'][0]['place_id']))
	else:
		return {'city':None,'country':None}
	
def gMapTextQueryCityTranslated(query,qualifier=None):
	untranslated = gMapTextQueryPlaceDetails(query, qualifier)
	translated = gMapTextQueryPlaceDetails(untranslated['city'] + ', ' + untranslated['country'])
	return translated
	
def gMapQueryType(name, city=None, state=None, type=None,qualifier=None):
	if name is not None:
		if qualifier is None:
			queryOut = (name+' '+city+' '+state).replace (" ", "+")
			
		elif type and qualifier:
			queryOut = (qualifier+' '+type+' ''''+name+' '''+city+' '+state).replace (" ", "+")
		return gMapTextQuery(queryOut,qualifier)
		
	else:
		return "Error bad query input"

def gMapDistance(origin, destination):  #to be in format latitute,longitude
	distanceURL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="+origin+"&destinations="+destination+"&key="+key	
	response = requests.get(distanceURL, headers=headers)
	result = json.loads(response.content.decode('utf-8'))
	#print(result)
	if len(result['rows'][0]) and result['rows'][0]['elements'][0]['status'] != 'ZERO_RESULTS':
		return result['rows'][0]['elements'][0]['distance']['text']
	else:
		return None


#print(gMapTextQueryCityTranslated('tower of london', 'point_of_interest'))

#print(gMapQueryType("snowbasin","huntsville","utah"))


#print(gMapDistance("41.230237,-111.8326101","41.1934195,-112.0089781"))