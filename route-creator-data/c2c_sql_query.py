#!/usr/bin/env python3
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2.extras import RealDictCursor
from contextlib import contextmanager
import envdir
import os

path = os.path.join(os.path.expanduser('~'), 'envdir/')
if not os.path.isdir(path):
    raise ValueError('%s is not a dir' % path)
envdir.Env(path)

WAREHOUSE_HOST = os.environ['WH_HOST']
WAREHOUSE_USER = os.environ['WH_USER']
WAREHOUSE_PWD = os.environ['WH_PWD']

def generate_kwargs():
	return {
		'host': WAREHOUSE_HOST,
		'port': 5439,
		'database': 'warehouse',
		'user': WAREHOUSE_USER,
		'password': WAREHOUSE_PWD
	}

@contextmanager
def get_cursor(connection_callable, autocommit=False, cursor_factory=RealDictCursor): # pragma: no cover
    connection = connection_callable()
    if autocommit:
        connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    try:
        with connection.cursor(cursor_factory=cursor_factory) as cursor:
            yield cursor
        connection.commit()
    finally:
        connection.close()


def warehouse_connection():
    conn_kwargs = generate_kwargs()
    return psycopg2.connect(**conn_kwargs)


@contextmanager
def warehouse_cursor(**kwargs):
    with get_cursor(warehouse_connection) as cur:
        yield cur


#print (SQL_YOU_WANT_TO_USE)

def getWarehouseQuery(SQL_YOU_WANT_TO_USE,data=None):
	
	with warehouse_cursor() as cursor:
		cursor.execute(SQL_YOU_WANT_TO_USE,data)
		query = (cursor.fetchall())
		return query
	
		

