import requests
import math
import csv

API_KEY = "eYolXfId5GQDQsx27c0xhp-jjE4HtI6TByHJDG9XRpgAu75ZaYeAqxWujbCjvMi2y9ZUZLtZXuTAGRFYhc1b64CmOyooU4liMXcJzoL1ASvVTOW0FQ0UvF03-A1BXHYx" #  Replace this with your real API key


API_HOST = 'https://api.yelp.com'
BUSINESS_PATH = '/v3/businesses/'

def search_business(location, category, term, offset=0, price='1,2,3,4', features=''):
    search_path = BUSINESS_PATH + 'search'
    url = API_HOST + search_path
    headers = {'Authorization': 'Bearer '+ API_KEY}
    payload = {'location': location, 'category': category, 'term': term, 'limit':50, 'offset': offset, 'price':price, 'features':features}

    response = requests.get(url, headers=headers, params=payload)

    return response.json()
    
def checkRating(biz, minRating):
    total = 0
    for entry in biz:
        if entry['rating'] >= minRating:
            total = total + 1
    return total
    
def betterThan(location, category, keyword, minRating, price='1,2,3,4', features=''):
    response = search_business(location, category, keyword, 0, price, features)
    total = response['total']
    loops = math.ceil(total/50)
    countOk = checkRating(response['businesses'],minRating)
    for i in range(1,loops):
        offset = 50*i
        responseX = search_business(location, category, keyword, offset, price, features)
        countOk = countOk + checkRating(response['businesses'],minRating)
    return countOk
    
def cityLoop(city):
    #cupcakes = betterThan(city, 'bakeries', 'cupcakes', 4)
    #takeOut = betterThan(city, 'restuarant','restuarant', 4, '1,2,3,4', 'Order Takeout')
    #adult = betterThan(city,'adult','Adult Sex Stores', 0)
    #massage = betterThan(city,'massage therapy','Massage Therapy',4,'1,2')
    #output = [city, cupcakes,takeOut,adult,massage]
    
    #danceClubs = betterThan(city, 'Dance Clubs', 'Dance Clubs', 4)
    #shopping = betterThan(city, 'Shopping', 'Women’s Clothing', 4)
    bars = betterThan(city, 'Bars', 'Bars', 4,'1,2' )
    output = [city, bars]
    return output
    
#cities = ['Detroit','Baltimore','Washington D.C.','Atlanta','Philadelphia','Milwaukee','New Orleans','Boston','Memphis','Minneapolis','Chicago','Tucson','Oakland','Columbus','Indianapolis','Kansas City','Denver','Los Angeles','Miami','Tampa','San Francisco','Seattle','Fresno','Long Beach','Austin','Raleigh','New York','Portland, OR','Albuquerque','Sacramento','Phoenix','Dallas','San Antonio','Charlotte','Nashville','Houston','Louisville','San Diego','Tulsa','Jacksonville','Arlington, TX','Las Vegas','Omaha','El Paso','Mesa','Oklahoma City','Fort Worth','Wichita','San Jose','Colorado Springs','Virginia Beach']

#cities = ['Las Vegas NV','Orlando FL','New York City NY','Phoenix AZ','Orange County CA','Miami FL','Chicago IL','New Orleans LA','San Diego CA','San Francisco CA','Los Angeles CA','San Antonio TX','Dallas TX','Washington D.C. ','Fort Lauderdale FL','Nashville TN','Atlanta GA','Houston TX','Seattle WA','Denver CO','Austin TX','Minneapolis MN','St. Petersburg FL','Boston MA','Fort Myers FL','St. Louis MO','Tampa FL','Savannah GA','The Florida Keys FL','Palm Springs CA','Portland OR','San Jose CA','Columbus OH','Daytona Beach FL','Atlantic City NJ','Kansas City MO','Idianapolis IN','Panama City FL','Oahu HI','Charleston SC','Sarasota FL','Monterey CA','Charlotte NC','Cincinnati OH','Asheville NC','Louisville KY','Santa Fe NM','Portland ME']

cities = ['Detroit','Baltimore','Washington D.C.','Atlanta','Philadelphia','Milwaukee','New Orleans','Boston','Memphis','Minneapolis','Chicago','Tucson','Oakland','Columbus, OH','Indianapolis','Kansas City, MO','Denver','Los Angeles','Miami','Tampa','San Francisco','Seattle','Fresno','Long Beach, CA','Austin','Raleigh','New York','Portland, OR','Albuquerque','Sacramento','Phoenix','Dallas','San Antonio','Charlotte','Nashville','Houston','Louisville','San Diego','Tulsa','Jacksonville','Arlington, TX','Las Vegas','Omaha','El Paso, TX','Mesa, AZ','Oklahoma City','Fort Worth','Wichita, KS','San Jose, CA','Colorado Springs','Virginia Beach','Asheville, N.C.','Cleveland','Honolulu','St. Louis','Pittsburgh','Cincinnati','Anchorage'
]

finalOut = []
finalOut.append(['City',])
for city in cities:
    cityOut = cityLoop(city)
    finalOut.append(cityOut)
    #print(finalOut)
   
with open("outputYelp.csv", 'w', newline='') as resultFile:
    wr = csv.writer(resultFile, dialect='excel')
    wr.writerows(finalOut)   
    