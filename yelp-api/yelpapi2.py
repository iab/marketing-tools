import io
import json
import pickle
import csv as csv
from yelp.client import Client
from yelp.oauth1_authenticator import Oauth1Authenticator

# read API keys
auth = Oauth1Authenticator(
    consumer_key='qcl9CCqYR7LVBJMAD4Hyew',
    consumer_secret='frtzJbsfN0LaFQTy7lUylR9HiTk',
    token='Y8-RKyusYNdw7AVlh2LEq1BKIqFHJG2v',
    token_secret='IXICsSyN_fM0dD1yc8Y5RSkCLWY'
)

client = Client(auth)

	
csv_Cities = csv.reader(open("inputCities.csv"),dialect='excel')
csv_Categories = csv.reader(open("inputCategories.csv"),dialect='excel')

list_Cities = list(csv_Cities)
list_Categories = list(csv_Categories)
headingsCategories =["City"]
for category in list_Categories:
	headingsCategories.append(category[0])
	headingsCategories.append(category[0]+" closed")

with open('yelpOutput.csv', 'w') as yelpOutput:
	csv_writer = csv.writer(yelpOutput,dialect='excel')
	csv_writer.writerow(headingsCategories)

	for city in list_Cities:
		outputRow = [city[0]]
		for category in list_Categories:
			
			params = {
				'category_filter': category[0],
				'location' :city[0]
			}
			
			response = client.search(**params)
			
			
			closedBusiness = 0
			list_responseBusinesses = list(response.businesses)
			for business in list_responseBusinesses:
				#print (business.name)
				#print (business.rating)
				#print (business.is_closed)
				if business.is_closed:
					closedBusiness += 1
			print(city[0]+ " had "+ str(response.total)  + " " + (category[0] )+ " closed business " + str(closedBusiness))
			outputRow.append(response.total)
			outputRow.append(closedBusiness)
		#print(outputRow)
		csv_writer.writerow(outputRow)	

		'''
		print business.name
		print business.rating
		print business.is_closed
		print "--------"
		'''