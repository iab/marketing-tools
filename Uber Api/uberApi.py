from uber_rides.session import Session
import csv
session = Session(server_token='b4f-EAXacGxY5cgw4NBdlqukXXbdVzbGv7MjTUEj')

from uber_rides.client import UberRidesClient
client = UberRidesClient(session)

with open('city-input.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    cityOut = []
    cityOut.append(['city','product','cost per minute', 'base price', 'min price', 'cost per distance'])
    for cityData in csv_reader:
        city = cityData[0]
        lat = cityData[2]
        long = cityData[3]

        response = client.get_products(lat, long)
        products = response.json.get('products')

        uberXL = list(filter(lambda person: person['short_description']=='UberXL', products))
        uberXL = uberXL[0]
        cityinfo = []
        if not uberXL:
            cityInfo = [city] 
           
        
        else:
            
            print('city - ', city, 'api -', uberXL['short_description'], 'minute price -', uberXL['price_details']['cost_per_minute'], 'min price -', uberXL['price_details']['minimum'],'mile price -', uberXL['price_details']['cost_per_distance']   )
        
            cityInfo = [city,uberXL['short_description'],uberXL['price_details']['cost_per_minute'],uberXL['price_details']['base'],uberXL['price_details']['minimum'],uberXL['price_details']['cost_per_distance'] ]
        
        cityOut.append(cityInfo)
        


  
with open("outputUber.csv", 'w', newline='') as resultFile:
    wr = csv.writer(resultFile, dialect='excel')
    wr.writerows(cityOut)   