import os, json
import pandas as pd
#import networkx as nx
from collections import Mapping
from distance_calc import haversine 
import csv
		
def load_stations():
	path_to_json = 'stations-gen.json'
	stationsCities = {}
	with open(path_to_json) as json_file:
		for station in json.load(json_file):
			wid = station['wid']
			wcityid = station['wcityid']
			stationsCities[wid] = wcityid
		return stationsCities

def cityRewrite(city):
	if city == 'RLA':
			city = 'ALB'
	return city		

def listify(json_in, cityMap, baseList):
	listHold = baseList
	for station in json_in:
		city = cityMap[station]
		city = cityRewrite(city)
		for connection in json_in[station]:
			city2 = cityMap[connection]
			city2 = cityRewrite(city2)
			if (city + '-' + city2) not in listHold:
				listHold.append(city + '-' + city2)
				#print ('added - '+ city + '-' + city2)
	return listHold

def write_routes(routes,name):
	myfile = open(name+'.csv', 'w')
	wr = csv.writer(myfile, quoting=csv.QUOTE_ALL, lineterminator='\n')
	wr.writerow(routes)		
	
def route_diff():
	path_to_json = 'wid-maps'
	path_to_json2 = 'gli-ppp-widmaps'
	json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]
	json_files2 = [pos_json for pos_json in os.listdir(path_to_json2) if pos_json.endswith('.json')]
	otherCarriersList = []
	gliCarrierList = []
	cityMap = load_stations()
	for js in json_files:
		with open(os.path.join(path_to_json, js)) as json_file:
			
			json_out = json.load(json_file)
			otherCarriersList = listify(json_out, cityMap, otherCarriersList)  
			#print('done loading '+str(js))
		print('next file - '+js)
	print('done with others')
	write_routes(otherCarriersList,'other-routes')
	for js in json_files2:
		with open(os.path.join(path_to_json2, js)) as json_file2:
			
			json_out2 = json.load(json_file2)
			gliCarrierList = listify(json_out2, cityMap, gliCarrierList)  
			#print('done loading '+str(js))
		print('next file - '+js)	
	write_routes(gliCarrierList,'gliCarrierList')
	print('length of others - '+ str(len(otherCarriersList)))
	print('length of gli - '+ str(len(gliCarrierList)))
	onlyGLI = []
	onlyGLI = list(set(gliCarrierList) - set(otherCarriersList))
	print(len(onlyGLI))
	write_routes(onlyGLI,'gli-diff')
	
	
	
route_diff()
