import os, json
import pandas as pd
import networkx as nx
from collections import Mapping
from distance_calc import haversine 
import csv

G = nx.Graph()
close_to_max_distance = 2 #miles

		
def load_stations():
	path_to_json = 'stations-gen.json'
	stationsCities = {}
	stationsLatLng = {}
	with open(path_to_json) as json_file:
		for station in json.load(json_file):
			wid = station['wid']
			wcityid = station['wcityid']
			lat = station['loc']['lat']
			lng = station['loc']['lng']
			stationsCities[wid] = wcityid
			stationsLatLng[wid] = {}
			stationsLatLng[wid]['lat'] = lat
			stationsLatLng[wid]['lng'] = lng
		return stationsCities,stationsLatLng

def initial_edges(json_in, latlngMap):  # get distance and add edge with weight = distance
	for station in json_in:
		lat1 = latlngMap[station]['lat']
		lng1 = latlngMap[station]['lng']
		for connection in json_in[station]:
			lng2 = latlngMap[connection]['lng']
			lat2 = latlngMap[connection]['lat']
			distance = haversine(lng1, lat1, lng2, lat2)
			G.add_edge(station,connection,length=distance)
			
def close_to(latlngMap):
	close_to_list = []
	for node in G.nodes():
		lat1 = latlngMap[node]['lat']
		lng1 = latlngMap[node]['lng']
		for node2 in G.nodes():
			if node != node2:
				lng2 = latlngMap[node2]['lng']
				lat2 = latlngMap[node2]['lat']
				distance = haversine(lng1, lat1, lng2, lat2) #find haversine distance
				if distance <= close_to_max_distance:  # distance less than close to max
					G.add_edge(node,node2,length=distance,close_to=True) # set distance to edge weight
					
					
def shortest_path():
	paths = nx.johnson(G, weight='length')
	found_routes = []
	three_legged = []
	for node in paths:
		for node2 in paths[node]:
			if len(paths[node][node2]) == 3:
				route = str(node)+'_'+str(node2)
				found_routes.append(route)
			if len(paths[node][node2]) == 4:
				route = str(node)+'_'+str(node2)
				if check_close_to(paths[node][node2]):
					route = str(node)+'_'+str(node2)
					found_routes.append(route)
					three_legged.append(route)
				
	return found_routes, three_legged
	
def convert_to_city_route(routes, cityMap):
	city_routes = []
	for route in routes:
		wid,wid2 = route.split('_')
		cityRoute = cityMap[wid]+'_'+cityMap[wid2]
		#print(cityRoute)
		if cityRoute not in city_routes:
			city_routes.append(cityRoute)
	return city_routes
	
def check_close_to(path):
	if ('close_to' in G[path[1]][path[2]]):
		return True
	
def check_c2c_routes(routes):  #go through non-routed wid maps and remove any from the routed route list
	f = open('c2c-wids.csv')
	print('c2c file loaded')
	c2c_routes = csv.reader(f)
	for row in c2c_routes:
		if row[0] in routes:
			#print('duplicate found'+row[0])
			routes.remove(row[0])
	print('de-duped routed c2cs are'+str(len(routes)))
			

def write_routes(routes):
	myfile = open('routed_routes.csv', 'w')
	wr = csv.writer(myfile, quoting=csv.QUOTE_ALL, lineterminator='\n')
	wr.writerow(routes)		
	
def path_finder():
	path_to_json = 'wid-maps'
	json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]
	cityMap, latlngMap = load_stations()
	for js in json_files:
		with open(os.path.join(path_to_json, js)) as json_file:
			
			json_out = json.load(json_file)
			initial_edges(json_out, latlngMap)  #buidling edges from existing trips
			#print('done loading '+str(js))
	close_to(latlngMap)  #creating close_to edges
	routes_out, three_legs = shortest_path()
	print('done w initial routes')
	#sanity checking delete
	
	print('num of edges ' + str(G.number_of_edges()))
	print('routes ' + str(len(routes_out)))
	print('3 hop routes ' + str(len(three_legs)))
	
	cityRoutes = convert_to_city_route(routes_out, cityMap)
	print('city Routes ok')
	print('number of non-duped new c2cs are ' + str(len(cityRoutes)))
	cityRoutesFinal = check_c2c_routes(cityRoutes)
	write_routes(cityRoutes)  #adjust to post check_c2c_routes writing

#add city name, state and country to cityMap dictionary to do MSV lookup	
#add msv lookup
#potentially add msv de-dupe w current c2c routes
	
path_finder()

#for 3 legged why are the shortest paths being built w wierd 2nd legs?? once fixed, check_close_to should return the list of routes that are close_to, then it should be added to the routes_out list and that will be routed trips list.
#once all routes are done, convert them into cities
# extract all c2c pages that now exist, find list of new pages that can be created
